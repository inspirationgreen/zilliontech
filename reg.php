<?php 
//ob_start(); 
$page='signup'; require_once 'overall/header.php';?>
    <div class="main-content">
        <div class="hero">
            <form method="POST">
                <div id="errors">
                  <?php require_once 'inc/register.inc.php';?>  
                </div >
                <div>
                <ul class="progressbar">
                    <li class="active">About you</li>
                    <li>Contact info</li>
                    <li>Account</li>
                </ul> 
                </div>
                 
                <div>
                    <label>First Name:</label>
                    <input type="text" name="f_name" placeholder="First Name" autocomplete="off">
                </div>
                <div>
                    <label>Last Name:</label>
                    <input type="text" name="l_name" placeholder="Last Name" autocomplete="off">
                </div>
                <div>
                    <label>Date of Birth:</label><br/>
                    <input type="date" name="date" autocomplete="off">
                </div>
                <div>
                    <label>Email:</label>
                    <input type="text" name="email" placeholder="Email" autocomplete="off">
                </div>
                <button class="btn_4" type="submit">Next</button> 
                <div class="clear"></div>
            </form>            
        </div>
<?php require_once 'overall/footer.php';?>
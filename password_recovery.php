<?php $page='login'; require_once 'overall/header.php';?>
    <div class="main-content">
        <div class="hero">
            <form method="POST">
                <div id="errors">
                    <?php require_once 'inc/password_recovery.inc.php';?>
                </div>
                <div>
                    <label>Enter your registered email:</label>
                    <input type="text" name="zt_email" placeholder="Email" autocomplete="off">
                </div>
                <div>
                    <label style="font-size: 14px;">Before you click the reset button please make sure you have access to the provided email. An activation code will be send it text to you registered mobile number.</label><br/>
                    </div>               
                <button class="btn_4" type="submit">Reset Password</button>                                
            </form>            
        </div>
<?php require_once 'overall/footer.php';?>
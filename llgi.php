<?php $page='login'; require_once 'overall/header.php';?>
    <div class="main-content">
        <div class="hero">
            <form method="POST">
                <div id="errors">
                    
                    <?php 
                    if(Session::exists('email_check')){
                        echo Session::flash('email_check');
                    }
                    require_once 'inc/llgi.inc.php';?>
                </div>
                <div>
                    <label>Username:</label>
                    <input type="text" name="zt_username" placeholder="ID or Username" autocomplete="off">
                </div>
                <div>
                    <label>Password:</label><br/>
                    <input type="password" name="user_pass" placeholder="Password" autocomplete="off">
                </div>
                    <div id="form_extra"><label><a href="password_recovery.php">Forgot your password?</a></label></div>
                    <input type="checkbox" name="remember" id="remember"/> 
                    <label for="remember">Remember Me</label>
               
                <button class="btn_4" type="submit">Sign In</button>
                                
            </form> 
            <div class="clear"></div>
        </div>
<?php require_once 'overall/footer.php';?>
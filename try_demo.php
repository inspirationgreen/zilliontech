<?php $page='demo'; require_once 'overall/header.php';?>
<?php require 'srv.php';?>
    <div class="main-content">
        <div class="main">
        <div id="simpleModal" class="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="closeBtn">&times;</span> 
                    <h2>New purchase confirmation</h2>                    
                </div>
                <div class="modal-body">
                    <p>To purchase this puzzle component ID: <span id="serial"></span> your credit must be above 25p.</p>
                    <p>Please confirm that you want purchase this component.</p>
                    <div>
                        <button class="yes_btn">Yes</button>
                        <button class="no_btn">No</button>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <h3>Modal Footer</h3>
                </div>
            </div>
        </div>
            <div class="left_side">
                <div class="component">
                    <div class="title" style="text-align: center;">Buy components</div>  
                    <div id="tickets" class="chart">
                        <?php require_once 'inc/component_list.inc.php';?>
                    </div>
                </div>
                <div class="component">
                    <div class="title" style="text-align: center;">All your Components</div>
                    <div id="shapes" class="chart" style="background: #CACFD6;">
                        <br>
                        <?php require_once 'inc/user_cmp_list.inc.php';?>
                    </div>
                </div>
            </div>
            <div class="main_area">
                <div class="chart" style="text-align:center;">
                    <div class="dashboard">
                        <div class="clock">
                            <span class="timming">02:00:00</span></br>
                            <span>Time left to play today</span>
                        </div>
                        <div class="credit">
                            <span>£</span><span id="money"><?php echo number_format(($credit-$credit_used)/100, 2);?></span></br>
                            <span>Your Credit</span>
                        </div>
                        <div class="earn">
                            <span>£120.00</span></br>
                            <span>You earned from trade</span>
                        </div>
                    </div>
                                        
                        <svg  style=" width:500px; height: 600px; margin-top: 25px; margin-left: auto; margin-right: auto; fill: #FF7F00">
                        <polygon class="st0" points="183.013,500 66.986,433.011 0,316.986 0,183.013 23.414,142.458 66.986,66.986 183.013,0 316.986,0 
                                                     433.011,66.986 500,183.013 500,316.986 433.011,433.011 316.986,500 "/>
                        </svg>
                        <div id="drop_zone">
                        <div id="new_element">
                            <span id="txt_span">New component to the game board.</span>
                        </div>                             
                        </div>
                        <!--<div class="clear"></div>-->
                    </div>
            </div>
            <div class="right_side">
                <div class="title" style="text-align: center;">Sale components</div>
                
            </div>
        </div>
<?php require_once 'overall/footer.php';?>


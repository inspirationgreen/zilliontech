<?php require_once 'overall/header_profile.php';?>
        <div class="user-main-content">
            <div class="title">
                Hello <?php echo escape($user->data()->f_name);?>, Welcome to the User Profile Dashboard!
            </div>
            <div class="main">
                <div class="widget">
                        <div class="title">Edit Profile</div>
                        <div class="chart">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer consequat mi id lacus iaculis, at blandit sapien pulvinar. Vivamus egestas mauris sed porta euismod. Nulla facilisi. Etiam vel tincidunt massa, a dapibus massa. Quisque eget felis mi. </p>
                        </div>
                </div>
                <div class="widget">
                        <div class="title">Try a Demo</div>
                        <div class="chart">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer consequat mi id lacus iaculis, at blandit sapien pulvinar. Vivamus egestas mauris sed porta euismod. Nulla facilisi. Etiam vel tincidunt massa, a dapibus massa. Quisque eget felis mi. </p>
                        </div>
                </div>
                <div class="widget">
                        <div class="title">Create a live account</div>
                        <div class="chart">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer consequat mi id lacus iaculis, at blandit sapien pulvinar. Vivamus egestas mauris sed porta euismod. Nulla facilisi. Etiam vel tincidunt massa, a dapibus massa. Quisque eget felis mi. </p>
                        </div>
                </div>
            </div>
            <div class="main">
                <div class="widget">
                        <div class="title">Add Credit to your account</div>
                        <div class="chart">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer consequat mi id lacus iaculis, at blandit sapien pulvinar. Vivamus egestas mauris sed porta euismod. Nulla facilisi. Etiam vel tincidunt massa, a dapibus massa. Quisque eget felis mi. </p>
                        </div>
                </div>
                <div class="widget">
                        <div class="title">Transfer money to your bank</div>
                        <div class="chart">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer consequat mi id lacus iaculis, at blandit sapien pulvinar. Vivamus egestas mauris sed porta euismod. Nulla facilisi. Etiam vel tincidunt massa, a dapibus massa. Quisque eget felis mi. </p>
                        </div>
                </div>
                <div class="widget">
                        <div class="title">See all Transactions</div>
                        <div class="chart">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer consequat mi id lacus iaculis, at blandit sapien pulvinar. Vivamus egestas mauris sed porta euismod. Nulla facilisi. Etiam vel tincidunt massa, a dapibus massa. Quisque eget felis mi. </p>
                        </div>
                </div>
            </div>
        </div>
<?php require_once 'overall/footer.php';?>
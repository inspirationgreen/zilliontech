<?php ob_start(); $page='signup'; require_once 'overall/header.php';?>
    <div class="main-content">
        <div class="hero">
            <form method="POST">
                <div id="errors">
                  <?php require_once 'inc/register_cnf.inc.php';?>  
                </div>
                <div>
                <ul class="progressbar">
                    <li class="active">About you</li>
                    <li class="active">Contact info</li>
                    <li>Account</li>
                </ul> 
                </div>                
                <div>
                    <label>Address:</label>
                    <input type="text" name="address" placeholder="Address" autocomplete="off">
                </div>
                <div>
                    <label>Postcode:</label><br/>
                    <input type="text" name="postcode" placeholder="Postcode" style="width: 50%;" autocomplete="off">
                </div>
                <div>
                    <label>Town / City:</label><br/>
                    <input type="text" name="town" placeholder="Town / City" autocomplete="off">
                </div>
                <div>
                    <label>Contact Number:</label><br/>
                    <input type="tel" name="phone" placeholder="Contact Number" autocomplete="off">
                </div>
                <button class="btn_4" type="submit">Next</button>                
            </form>            
        </div>
<?php require_once 'overall/footer.php';?>
<?php
ob_start();
require_once 'core/zt_init.php'; 
$user = new User();
if(!$user->isLoggedIn()){
    Redirect::to('llgi.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initioal-scale=1">
        <link rel="stylesheet" type="text/css" href="css/screen.css">
        <link rel="stylesheet" type="text/css" href="css/form_style.css">
        <link rel="stylesheet" type="text/css" href="css/cnv_style.css">
        <link rel="stylesheet" type="text/css" href="css/side_nav_style.css">
        <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
        <script src="js/main.js"></script>
        <title>ZillionTech</title>
    </head>
    <body>
        <div class="header">
            <div class="logo"><h1>
                    <span style="color: #FF7F00">Zillion</span>Tech
                </h1>
            </div>
            <a href="#" class="nav-trigger"><span></span></a>
        </div>
            <div class="side-nav">
                <nav>
                    <ul>
                        <li class="active">
                            <a href="edit_pro.php">
                                <span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                                <samp>Edit Profile</samp>
                            </a>
                        </li>
                        <li>
                            <a href="demo_actionscript.php">
                                <span><i class="fa fa-gamepad" aria-hidden="true"></i></span>
                                <samp>Try a Demo</samp>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span><i class="fa fa-line-chart" aria-hidden="true"></i></span>
                                <samp>Get Live Account</samp>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span><i class="fa fa-credit-card-alt" aria-hidden="true"></i></span>
                                <samp>Add Credit</samp>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span><i class="fa fa-money" aria-hidden="true"></i></span>
                                <samp>Credit Transfer</samp>
                            </a>
                        </li>
                        <li>
                            <a href="index.php">
                                <span><i class="fa fa-home" aria-hidden="true"></i></span>
                                <samp>Home</samp>
                            </a>
                        </li>
                    </ul>
                </nav>
                
            </div>
<?php
ob_start();
require_once 'core/zt_init.php';?>
<?php require 'srv.php';?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initioal-scale=1">
        <link rel="stylesheet" type="text/css" href="css/screen.css">
        <link rel="stylesheet" type="text/css" href="css/animation.css">
        <link rel="stylesheet" type="text/css" href="css/demo.css">
        <link rel="stylesheet" type="text/css" href="css/form_style.css">
        <link rel="stylesheet" type="text/css" href="css/cnv_style.css">
        <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/2.6.4/svg.js"></script>-->
        <script src="js/main.js"></script>
        <title>ZillionTech</title>
        <style>
            /*<![CDATA[*/
                .st1{fill:#264E9D;stroke:#FFFFFF;stroke-miterlimit:10;}
             /*   ]]>*/
        </style>
    </head>
    <body>
        <div class="header">
            <div class="logo"><h1>
                    <span style="color: #FF7F00">Zillion</span>Tech
                </h1>
            </div>
            <a href="#" class="nav-trigger"><span></span></a>
            <div class="top-nav">
                <nav>
                    <ul>
                        <li class="<?php if($page =='home'){echo 'active';}?>">
                            <a href="index.php">
                                <span><i class="fa fa-home" aria-hidden="true"></i></span>
                                <samp>Home</samp>
                            </a>
                        </li>
                        
                        <?php 
                        $user = new User();
                        if ($user->isLoggedIn()){
                        ?>                       
                        <li class="<?php if($page =='profile'){echo 'active';}?>">
                            <a href="user_profile.php">
                                <span><i class="fa fa-user-circle-o" aria-hidden="true"></i></span>
                                <samp>My account</samp>
                            </a>
                        </li>
                        <li class="<?php if($page =='demo'){echo 'active';}?>">
                            <a href="try_demo.php">
                                <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                <samp>Try Demo</samp>
                            </a>
                        </li>
                        <li class="<?php if($page =='logout'){echo 'active';}?>">
                            <a href="logout.php">
                                <span><i class="fa fa-sign-out" aria-hidden="true"></i></span>
                                <samp>Log Out</samp>
                            </a>
                        </li>
                        <li class="<?php if($page =='contact'){echo 'active';}?>">
                            <a href="contact.php">
                                <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                <samp>Contact</samp>
                            </a>
                        </li>
                        <?php } else{?>                        
                         <li class="<?php if($page =='demo'){echo 'active';}?>">
                            <a href="demo_actionscript.php">
                                <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                <samp>Try Demo</samp>
                            </a>
                        </li>
                        <li class="<?php if($page =='contact'){echo 'active';}?>">
                            <a href="contact.php">
                                <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                <samp>Contact</samp>
                            </a>
                        </li>
                        <li class="<?php if($page =='login'){echo 'active';}?>">
                            <a href="llgi.php">
                                <span><i class="fa fa-sign-in" aria-hidden="true"></i></span>
                                <samp>Log In</samp>
                            </a>
                        </li>
                        <?php }?>
                    </ul>
                    
                </nav>
                
            </div>
        </div>


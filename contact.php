<?php ob_start(); $page='contact'; require_once 'overall/header.php';?>

    <div class="main-content">
        <div class="hero">
            <form method="POST">
                <div id="errors">
             <?php require_once 'inc/contact.inc.php'; ?>  
                </div>
                <div>
                    <label>First Name:</label>
                    <input type="text" name="f_name" placeholder="First Name" autocomplete="off">
                </div>
                <div>
                    <label>Email:</label><br/>
                    <input type="text" name="email" placeholder="Email" autocomplete="off">
                </div>
                <div>
                    <label>Message:</label><br/>
                    <textarea name="message" placeholder="Enter you mesage here" autocomplete="off"></textarea>
                </div>
                <input type="hidden" name="token" value="<?php  echo Token::generate(); ?>"/>
                <button class="btn_4" type="submit">Send</button>
                
            </form>            
        </div>
<?php require_once 'overall/footer.php';?>
<?php $page='login'; require_once 'overall/header.php';?>
    <div class="main-content">
        <div class="hero">
            <form method="POST">
                <div id="errors">
                    <?php require_once 'inc/zt_activate.inc.php';?>
                </div>
                <div>
                    <label>Activation Code:</label>
                    <input type="text" name="activation_code" placeholder="Enter activation code" autocomplete="off">
                </div>              
                <button class="btn_4" type="submit">Activate Account</button>
                <input type="hidden" name="token" value="<?php  echo Token::generate(); ?>"/>
            </form>            
        </div>
<?php require_once 'overall/footer.php';?>
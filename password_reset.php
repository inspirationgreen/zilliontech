<?php $page='login'; require_once 'overall/header.php';?>
    <div class="main-content">
        <div class="hero">
            <form method="POST">
                <div id="errors">
                    <?php require_once 'inc/password_reset.inc.php';?>
                </div>
                <div>
                    <label>Authentication code</label>
                    <input type="text" name="reset_code" placeholder="Enter the 8 didgits code" autocomplete="off" value="<?php echo Input::get('reset_code')?>">
                    <div id="form_extra" style="margin: 0; ;"><label style="font-size: 13px; color:#DEDEDE">The authentication code has been sent to your registered email address.</label></div>
                </div>
                <div>
                    <label>Date of Birth</label><br/>
                    <input type="date" name="date"  autocomplete="off" value="<?php echo Input::get('date')?>">
                    <div id="form_extra" style="margin: 0; ;"><label style="font-size: 13px; color:#DEDEDE">Click next to reset your password</label></div>
                </div>
                <div>
                    <label>Enter new Password:</label><label style="font-size: 13px; color:#DEDEDE"> Minimum 7 characters long</label><br/>
                    <input type="password" name="new_pass" placeholder="Password" autocomplete="off">
                </div>
                <div>
                    <label>Retype new Password:</label><br/>
                    <input type="password" name="new_pass_again" placeholder="Password" autocomplete="off">
                </div>
                <button class="btn_4" type="submit">Reset Password</button>                                
            </form>            
        </div>
<?php require_once 'overall/footer.php';?>
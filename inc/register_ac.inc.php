<?php
if (!Session::exists('address')){
    Redirect::to('reg.php');
} else{
    function randomString($lenght){
        $chars = "abcdefghijkmnlopqrstuvwxyz0123456789";
        srand ((double)microtime()* 1000000);
        $str ="";
        $i =1;
            while($i <=$lenght){
                $num = rand() %33;
                $tmp = substr($chars, $num, 1);
                $str = $str . $tmp;
                $i++;
        }
    return $str;
 }
$activation_code = randomString(rand(6, 6));
}
if(Input::exists()){
    $validate = new Validate();
$validation = $validate->check($_POST, array(
     'zt_username'=> array(
        'name'=> 'ID or Username',
        'required' => true,
        'min' => 6,
        'max' =>20,
        'unique' => 'zt_user'
     ),
    'user_pass' => array(
        'name' => 'Password',
        'required' => true,
        'min' => 6,
        'max' =>20
    ),
    'user_pass_again' => array(
        'name' => 'Password Again',
        'required' => true,
        'matches' => 'user_pass'
    )
));
    if($validation->passed()){ 
        $inputdob=date("Y-m-d", strtotime($_SESSION['date']));
        $user = new User();
           try{
               $user->create(array(
                   'f_name'         =>$_SESSION['f_name'],
                   'l_name'         =>$_SESSION['l_name'],
                   'user_dob'       =>$inputdob,
                   'zt_email'       =>$_SESSION['email'],
                   'address'        =>$_SESSION['address'],
                   'postcode'       =>$_SESSION['postcode'],
                   'town'           =>$_SESSION['town'],
                   'phone'          =>$_SESSION['phone'],
                   'zt_username'    => Input::get('zt_username'),
                   'user_pass'      =>password_hash(Input::get('user_pass'), PASSWORD_BCRYPT), 
                   'joined'         =>date('Y-m-d H:i:s'),
                   'zt_group'       => 1,
                   'activate_code'  => $activation_code
               ));
               $name = $_SESSION['f_name'].' '. $_SESSION['l_name'];
               $email = $_SESSION['email'];
                $subject = "Important Sign-up information ";
                $headers = "From:info@zilliontech.co.uk\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                $message = '<html><body>';
                $message .='<h2>Hellow,'.' '. $name .'</h2>';
                $message .='<p>Please use the following code to Activate you account.</p>';
                $message .='<p><h2>'.$activation_code.'</h2></p>'; 
                $message .='<p>Thank you.</p>'; 
                $message .='</body></html>'; 
                mail($email, $subject, $message, $headers);
               Session::flash('activate', 'You have been register, please log in and activate you account');
               $login = $user->login(Input::get('zt_username'), Input::get('user_pass'), $remember);                
              unset($_SESSION['f_name'], 
                    $_SESSION['l_name'], 
                    $_SESSION['date'],
                    $_SESSION['email'],
                    $_SESSION['address'],
                    $_SESSION['postcode'],
                    $_SESSION['town'],
                    $_SESSION['phone']
              );
              Redirect::to('llgi.php');
           } catch (Exception $e){
               die($e->getMessage());
           }
    } else {                   
        foreach($validation->errors() as $error){
        echo '<span style="color:#f00">'. $error. '</span><br>';
        }
    }
}
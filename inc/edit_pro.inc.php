 <?php 
    if(isset($_POST['s_email'])){
            if(Input::exists()){
                $validate = new Validate();
                $validation = $validate->check($_POST, array(
                    'email' => array(
                        'name'=> 'Email',
                        'required'=> true,
                        'min' => 5,
                        'max' =>30
                    )
                ));
                if($validation->passed()){
                    $user->update(array(
                        'zt_email' => Input::get('email')
                    ));
                    Redirect::to('edit_pro.php');
                }else {
                    foreach($validation->errors() as $error){
                    echo '<span style="color:#f00">'. $error. '</span><br>';
                }
            }
        }
    }
    if(isset($_POST['s_phone'])){
            if(Input::exists()){
                $validate = new Validate();
                $validation = $validate->check($_POST, array(
                    'phone' => array(
                        'name'=> 'New contact number',
                        'required'=> true,
                        'min' => 11,
                        'max' =>12
                    )
                ));
                if($validation->passed()){
                    $user->update(array(
                     'phone' => Input::get('phone')
                    ));
                    Redirect::to('edit_pro.php');
                }else {
                    foreach($validation->errors() as $error){
                    echo '<span style="color:#f00">'. $error. '</span><br>';
                }
            }
        }
    }
    if(isset($_POST['s_address'])){
            if(Input::exists()){
                $validate = new Validate();
                $validation = $validate->check($_POST, array(
                        'address' => array(
                            'name'=> 'First line of Address',
                            'required' => true
                        ),
                         'postcode' => array(
                             'name' => 'Your Home Postcode ',
                            'required' => true
                        ),
                         'town' => array(
                             'name' => 'Town / City',
                            'required' => true
                        )
                ));
                if($validation->passed()){
                    $user->update(array(
                        'address' => Input::get('address'),
                        'postcode'=> Input::get('postcode'),
                        'town'   => Input::get('town')
                    ));
                        
                }else {
                    foreach($validation->errors() as $error){
                    echo '<span style="color:#f00">'. $error. '</span><br>';
                }
            }
        }
    }
    if(isset($_POST['s_pass'])){
        if(Input::exists()){
            $validate = new Validate();
            $validation=$validate->check($_POST, array(
                'current_pass' => array(
                    'name'     => 'Your current Password',
                    'required' => true,
                    'min'      => 6,
                    'max'      =>12
                ),
                'new_pass' =>array(
                    'name'      =>'New password',
                    'required' => true,
                    'min'      => 6,
                    'max'      =>12
                ),
                'new_pass_again' =>array(
                    'name' => 'New password again',
                    'required' => true,
                    'matches'   => 'new_pass'
                )
            ));
            if($validation->passed()){
                if(password_verify(Input::get('current_pass'), $user->data()->user_pass)){
                    $user->update(array(
                        'user_pass'=>password_hash(Input::get('new_pass_again'), PASSWORD_BCRYPT)
                    ));
//                   Session::flash('pass_change', 'Your password has changed successfully! Now you can log in using the new password.');
                    $user->logout();
                    Redirect::to('edit_pro.php');
                
                } else{
                    echo '<span style="color:#ff0000;">Your Current Password is Incorect!</span><br>';
                }
                              
            }else {
                    foreach($validation->errors() as $error){
                    echo '<span style="color:#f00">'. $error. '</span><br>';
                }
            }
        }                    
    }


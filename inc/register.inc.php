<?php  
if($user->isLoggedIn()){
    $user->logout();
    Redirect::to('reg.php');
} else {
    $n_date = strtotime(date("Y-m-d"));
    if(Input::exists()){
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'f_name' => array(
                'name' => 'First Name',
                'required' => true,
                'min' => 3,
                'max' =>20
            ),
            'l_name' => array(
                'name'=> 'Last Name',
                'required'=> true,
                'min' => 3,
                'max' =>20
            ),
            'date' => array(
                'name' => 'DOB',
                'required' => true,
                'age' => strtotime('- 18 year', $n_date)
            ),
            'email' => array(
                'name'=> 'Email',
                'required'=> true,
                'min' => 3,
                'max' =>50
            )
        )); 
        if($validation->passed()){
            $email_check = DB::getInstance()->get('zt_user', array('zt_email', '=', Input::get('email')));
            if($email_check->count()){
                Session::flash('email_check', '<span style="color:#f00"><p>You all ready have an account. Please use this form to login.</span> ');
                Redirect::to('llgi.php');
            }else{            
           $_SESSION['f_name']= Input::get('f_name');
           $_SESSION['l_name']= Input::get('l_name');
           $_SESSION['date']= Input::get('date');
           $_SESSION['email']= Input::get('email');
            Redirect::to('reg_cnf.php');
            }
        } else {
        foreach($validation->errors() as $error){

            echo '<span style="color:#f00">'. $error. '</span><br>';
            }
        }
    }
}


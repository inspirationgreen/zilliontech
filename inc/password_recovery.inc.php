<?php
if(Input::exists()){
    $validate= new Validate();
    $validation= $validate->check($_POST, array(
    'zt_email'=>array(
        'name' => 'Email assress',
        'required'  => true,
        'min'       => 5
    )
    ));
    if($validation->passed()){
        $check_email = DB::getInstance()->get('zt_user', array('zt_email', '=', Input::get('zt_email')));
        if(!$check_email->count()){
            echo 'This Email don\'t exist!' ;
        } else{
            $user_id = $check_email->first()->id;
            $email = $check_email->first()->zt_email;
            $name = $check_email->first()->f_name;
            $generated_password = substr(md5(rand(999, 999999)), 0, 8);
            DB::getInstance()->update('zt_user',$user_id, array(
                'reset_token' => $generated_password
            ));
            $subject = "Password Reset";
            $headers = "From:info@zilliontech.co.uk\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $message = '<html><body>';
            $message .='<h2>Hellow,'.' '. $name .'</h2>';
            $message .='<p>Please use the following code to Reset your password.</p>';
            $message .='<p><h2>'.$generated_password.'</h2></p>'; 
            $message .='<p>Thank you.</p>'; 
            $message .='</body></html>'; 
            mail($email, $subject, $message, $headers);
            Redirect::to('password_reset.php');
        }
    }
}
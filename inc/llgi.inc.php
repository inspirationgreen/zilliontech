<?php
$user= new User();
if($user->isLoggedIn()){
    $activate = $user->data()->activate;
        if ($activate == 0){
              Session::flash('activate1', 'Please check your email and activate you account');
                Redirect::to('zt_activate.php');
        }else{
            Redirect::to('index.php');
        }
} else{
    if(Input::exists()){
        $validate= new Validate();
            $validation= $validate->check($_POST, array(
            'zt_username'=>array(
                'name' => 'ID or Username',
                'required'  => true,
                'min'       => 3
            ),
            'user_pass'=>array(
                'name' => 'Password',
                'required'  => true,
                'min'       => 6
            )

            ));
        if($validation->passed()){
            $user = new User();
                $remember = (Input::get('remember') === 'on') ? true : false;
                $login = $user->login(Input::get('zt_username'), Input::get('user_pass'), $remember);
               if($login){
                     $activate = $user->data()->activate;
                     if ($activate == 0){
                         Session::flash('activate1', 'Please check your email and activate you account');
                         Redirect::to('zt_activate.php');
                     } else {
                          Redirect::to('index.php');
                     }
                     echo 'success!';
                 } else {
                     echo '<span style="color:#f00"> Sorry, logging in failed</span>';
                 }
             }else {
             foreach($validation->errors() as $error){
             echo '<span style="color:#f00">'. $error. '</span><br>';
             }
        }
    }
}
?>
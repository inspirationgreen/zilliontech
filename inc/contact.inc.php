<?php
if(Input::exists()){
    if(Token::check(Input::get('token'))){
        $validate = new Validate();
        $validation =$validate->check($_POST, array(
            'f_name'=> array(
                'name'      =>'Your first name',
                'required'  => true,
                'min'       => 3,
                'max'       => 20
            ),
            'email' => array(
                'name'      => 'Email address',
                'required'  => true,
                'min'       => 6,
                'max'       => 50
            ),
            'message' => array(
               'name'       => 'Your Query message',
               'required'   => true,
               'min'        => 10,
               'max'        =>500
            )
        ));
        if($validation->passed()){
            echo 'ok!';
        } else{
            foreach($validation->errors() as $error){
                echo '<span style="color:#f00">'. $error. '</span><br>';
            }
        }
    }
}

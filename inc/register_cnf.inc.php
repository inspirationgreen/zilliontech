<?php
if (!Session::exists('f_name')){
    Redirect::to('reg.php');
}
if(Input::exists()){
    $validate = new Validate();
    $validation = $validate->check($_POST, array(
        
        'address' => array(
            'name'=> 'First line of Address',
            'required' => true
        ),
         'postcode' => array(
             'name' => 'Your Home Postcode ',
            'required' => true
        ),
         'town' => array(
             'name' => 'Town / City',
            'required' => true
        ),
         'phone' => array(
             'name' => 'Your Contact Number',
            'required' => true
        )
    ));
    if($validation->passed()){
        echo 'Success!';
       $_SESSION['address']= Input::get('address');
       $_SESSION['postcode']= Input::get('postcode');
       $_SESSION['town']= Input::get('town');
       $_SESSION['phone']= Input::get('phone');
        
        Redirect::to('reg_ac.php');
    } else {
                    
        foreach($validation->errors() as $error){

        echo '<span style="color:#f00">'. $error. '</span><br>';
        }
    }
}
<?php 
$user = new User();
if(!$user->isLoggedIn()){
    Redirect::to('llgi.php');
} else {
    if($user->data()->activate ==1){
        Redirect::to('llgi.php');
    } else{
        $activation_code = $user->data()->activate_code;
        if(Input::exists()){
            if(Token::check(Input::get('token'))){
                $validate = new Validate();
                $validation =$validate->check($_POST, array(
                    'activation_code' => array(
                        'name'     => 'Activation code',
                        'required' => true,
                        'min'      => 6,
                    )
                ));
                if($validation->passed()){
                    if(Input::get('activation_code')==$activation_code){
                        $user->update(array(
                            'activate'=>1,
                            'activate_code'=>''
                        ));
                        Session::flash('activated', 'Your account is now activated');
                        Redirect::to('index.php');
                    } else{
                        echo '<span style="color:#f00">Invalide code try again</span>';
                    }
                } else{
                    foreach($validation->errors() as $error){
                        echo '<span style="color:#f00">'. $error. '</span><br>';
                    }
                }
            }
        }
    }
}

?>
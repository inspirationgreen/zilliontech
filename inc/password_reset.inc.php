<?php
if(Input::exists()){
    $validate= new Validate();
    $validation= $validate->check($_POST, array(
    'reset_code'=>array(
        'name' => 'The authentication code',
        'required'  => true,
        'min'       => 8
    ),
    'date'=> array(
        'name' => 'DOB',
        'required'  => true
    ),
    'new_pass' =>array(
        'name' => 'New Password',
        'required'  => true,
        'min'       =>7,
        'max'       =>15
    ),
    'new_pass_again' =>array(
        'name' => 'Retype new Password ',
        'required'  => true,
        'matches' => 'new_pass'
    )
    ));
    if($validation->passed()){
        $find_user = DB::getInstance()->get('zt_user', array('reset_token', '=', Input::get('reset_code')));
        if(!$find_user->count()){
            echo '<span style="color:#f00">Invalid authentication code, Please try again.</span><br>';
        } else{
            $user_dob = $find_user->first()->user_dob;
            $username = $find_user->first()->zt_username;
            $user_id = $find_user->first()->id;
            if($user_dob != Input::get('date')){
                echo '<span style="color:#f00">The information does not match our records. Please <a href="contact.php">Click here</a> if you want to contact us.</span>';
            } else{
                $new_password = Input::get('new_pass_again');
                $user = new User();
                $login = $user->login($username, $new_password);
                DB::getInstance()->update('zt_user',$user_id, array(
                        'user_pass'=>password_hash($new_password, PASSWORD_BCRYPT)
                    ));
                    Redirect::to('index.php');
            }
        }
    } else {
        foreach($validation->errors() as $error){
        echo '<span style="color:#f00">'. $error. '</span><br>';
        }
    }
}

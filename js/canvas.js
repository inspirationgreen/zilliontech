var canvas = document.querySelector('canvas');
fitToContainer(canvas);

function fitToContainer(canvas){
  // Make it visually fill the positioned parent
  canvas.style.width ='100%';
  canvas.style.height='100%';
  // ...then set the internal size to match
  canvas.width  = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
}
var c = canvas.getContext("2d");

//c.arc(300, 300, 30, 0, Math.PI*2, false);
//c.stroke();
//for(var i=0; i<1000; i++){
//    var color = '#'+ Math.round(0xffffff * Math.random()).toString(16);
//    var x = Math.random()*window.innerWidth;
//    var y = Math.floor(Math.random() * 400);
//    c.fillStyle=color;
//    c.fillRect(x,y,100,20);  
//}
function Boxe(x, y, dy, color){
    this.x = x;
    this.y = y;
    this.dy = dy;
    this.color= color;
    this.draw = function(){
        var color = '#'+ Math.round(0xffffff * Math.random()).toString(16);
        c.fillStyle=this.color;
        c.fillRect(this.x,this.y,100,20); 
    }
    this.update = function(){
       if (this.x > innerWidth+100){
        this.x=0;
        }

        this.x+=10;
        if (this.y+10 >400|| this.y<0){
            this.dy=-this.dy;
        }
        this.y+=this.dy; 
        this.draw();
        }
}
var boxeArray = [];
for(var i =0; i<10; i++){
    var x =20;
    var y =10;
    var dy=10;
    var color = '#'+ Math.round(0xffffff * Math.random()).toString(16);
    boxeArray.push(new Boxe(x, y, dy, color));
}
console.log(boxeArray);
function moveThings(){
    requestAnimationFrame(moveThings);
    c.clearRect(0, 0, innerWidth, innerHeight);
    for(var i = 0; i<boxeArray.length; i++){
        boxeArray[i].update();
    }
}
moveThings();




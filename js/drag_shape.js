var id;
var f = function(){
    function Draggable(element, dragStart, dragDrop){
        this.element = element;
        this.dragStart = dragStart;
        this.dragDrop = dragDrop;
//        this.element.classList.add('draggable');
//        this.element.setAttribute("style","z-index:1;");
        var self = this;
        var move = function(event){
            if(self.dragStart !== undefined){ self.dragStart();}
//            console.log(event.type);
//            console.log(event.currentTarget);
            event.stopPropagation();
            //prevent any default action
            event.preventDefault();
            var originalLeft = parseInt(window.getComputedStyle(this).left);
            var originalTop = parseInt(window.getComputedStyle(this).top);
            
            var mouseDownX = event.clientX;
            var mouseDownY = event.clientY;
            
            function dragMe(event){
                self.element.style.left = originalLeft + event.clientX - mouseDownX + "px";
                self.element.style.top = originalTop + event.clientY - mouseDownY + "px";
                event.stopPropagation();
            };
//            document.getElementById('drop_zone').addEventListener("dragover",dragMe,true);
            function dropMe(event){
                document.removeEventListener('mousemove',dragMe,true);
                document.removeEventListener('mouseup',dropMe,true);
                if(self.dragDrop !== undefined){ self.dragDrop();}
                event.stopPropagation();
            }            
            document.addEventListener('mouseup',dropMe, true);
            document.addEventListener('mousemove',dragMe, true);                        
        };
        this.element.addEventListener('mousedown',move,false);
    };
    var dragStart = function(){
        id= this.element.id;
        var new_shape = document.getElementById(id);
        console.log(new_shape);
        document.getElementById('drop_zone').addEventListener("dragover", function(event){
            event.preventDefault();
        }, false);    
        document.getElementById('new_element').style.display = 'none';
    };
    var dragDrop = function(){
        var div = document.getElementById('drop_zone').contains(this.element);
        if(div ===false){
        var the_shape = document.getElementById(id);
        var shape_list =document.getElementById('drop_zone');
        the_shape.setAttribute("style", "margin-left:3px 0 0 20px");
        the_shape.setAttribute("style", "position:absolute;");
        document.getElementById('drop_zone').appendChild(this.element);
        document.getElementById('new_element').style.display = 'block';
        var br = document.getElementsByTagName('br')[1];
        shape_list.removeChild(br);
        }
    };
    var x = document.getElementById("shapes").childElementCount;
       var no_poly = (x-1)/2;
       if(x > 2 ){
       for (i = 0; i < no_poly; i++) {
       document.getElementsByClassName('objects')[i].setAttribute("draggable", true); 
       document.getElementsByClassName('objects')[i].classList.add('draggable');
      var img_shape = document.getElementsByClassName('objects')[i];
       img_shape.style.position = "relative";  
       img_shape = new Draggable(img_shape, dragStart, dragDrop);
       }    
    }
};
window.addEventListener('load',f,false);

    

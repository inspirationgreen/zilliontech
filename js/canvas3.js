var canvas = document.querySelector('canvas');
fitToContainer(canvas);

function fitToContainer(canvas){
  // Make it visually fill the positioned parent
  canvas.style.width ='100%';
  canvas.style.height='100%';
  // ...then set the internal size to match
  canvas.width  = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
}
var c = canvas.getContext("2d");

function Circle(x, y, dx, dy, w, h, color, angle, $){
    this.x= x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.w = w;
    this.h = h;
    this.color = color;
    this.angle +=1;
//    $(this).rotate({animateTo:angle});
    this.draw = function(){
//        c.beginPath();
        c.fillStyle=this.color;
        c.fillRect(this.x, this.y, this.w, this.h);

    }

    this.update = function(){
         if(this.x + this.w > innerWidth){
             this.x=0;
         } 
//         if( this.x -this.w<0){
//             this.x=innerWidth;
//         }
         if(this.y + this.h> 400 || this.y - this.h < 0){
             this.dy = - this.dy;
         }
         this.x +=1;
         this.y +=this.dy;
         this.draw();
        
    }
}
var circleArray=[];
for(var i=0; i<25; i++){
//var circle = new Circle(200, 200, 3, 3, 30);
var x = Math.random()*innerWidth;
var y = Math.random()*innerHeight;
var dx = (Math.random() - 0.5);
var dy = (Math.random() - 0.5);
var w = 20;
var h = 20;
var color = '#'+ Math.round(0xffffff * Math.random()).toString(16);
circleArray.push(new Circle(x, y, dx, dy, w, h, color));
}
console.log(circleArray);

function animate(){
    requestAnimationFrame(animate);
    c.clearRect(0, 0, innerWidth, innerHeight);
    for(var i = 0; i<circleArray.length; i++){
        circleArray[i].update();
        
    }
}
animate();
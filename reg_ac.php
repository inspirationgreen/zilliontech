<?php 
//ob_start();
$page='signup'; 
require_once 'overall/header.php';?>
    <div class="main-content">
        <div class="hero">
            <form method="POST">
                <div id="errors">
                    <?php require_once 'inc/register_ac.inc.php'; ?>  
                </div>
                <div>
                <ul class="progressbar">
                    <li class="active">About you</li>
                    <li class="active">Contact info</li>
                    <li class="active">Account</li>
                </ul> 
                </div>
                
                <div>
                    <label>Username:</label>
                    <input type="text" name="zt_username" placeholder="ID or Username" autocomplete="off">
                </div>
                <div>
                    <label>Password:</label><br/>
                    <label style="font-size: 13px; color:#DEDEDE"> Minimum 7 characters long.</label><br/>
                    <input type="password" name="user_pass" placeholder="Password" autocomplete="off">
                </div>
                <div>
                    <label>Enter the password again:</label><br/>
                    <input type="password" name="user_pass_again" placeholder="Enter the password again" autocomplete="off">
                </div>
                    <input type="checkbox" name="tandc" id="tandc"/> 
                    <label for="tandc">Terms and Conditions.</label>               
                <button class="btn_4" type="submit">Register</button> 
                <div class="clear"></div>
            </form>            
        </div>
<?php require_once 'overall/footer.php';?>
<?php $page='home'; require_once 'overall/header.php';?>
        <div class="main-content">
            <div class="hero">
                <div id="comments">
                    <h1>Welcome to ZillionTeck</h1>
                    <div class="graphic1">
                        <img src="images/home_screen.svg">
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultrices dictum tempus. Duis id sem vulputate, aliquam eros vitae, dignissim diam. Fusce mattis magna nulla.</p>
                    <ul>
                        <li class="btn_2">
                            <a href="about.php">
                            <span>What is ZillionTech</span>
                            </a>
                        </li>
                        <li class="btn_3">
                            <a href="reg.php">
                            <span>Sign Up</span>
                            </a>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
<?php require_once 'overall/footer.php';?>

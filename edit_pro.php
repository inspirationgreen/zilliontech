<?php require_once 'overall/header_profile.php';?>
    <div class="main-content">
        <div class="pro_main">
        <div class="pro_widget" >
                <div class="title" style="text-align: center">Edit your profile information.</div> 
            </div>
        </div>        
        <div class="pro_main">
            <div class="pro_widget" >
                <div id="errors">
                    <?php require_once 'inc/edit_pro.inc.php';?>
                </div>
            </div>   
        </div>
        <div class="pro_main">
            <div class="pro_widget" style="flex-basis: 300px;" id="pro_pass">
                <form method="POST">
                    <div>
                        <label>Current Password:</label>
                        <input type="password" name="current_pass" placeholder="Enter current password" autocomplete="off">
                    </div>
                    <div>
                        <label>New Password:</label><br/>
                        <input type="password" name="new_pass" placeholder="Enter the New Password"  autocomplete="off">
                    </div>
                    <div>
                        <label>New password again:</label><br/>
                        <input type="password" name="new_pass_again" placeholder="Enter the New Password again" autocomplete="off">
                    </div>
                          <button class="btn_4" name="s_pass" type="submit">Save</button>                      
                </form>
                <hr style="width: 100%;">
            </div>
            <div class="pro_widget" style="flex-basis: 300px;" id="pro_address">
                  <form method="POST">
                <div>
                    <label>Address:</label>
                    <input type="text" name="address" placeholder="Address" autocomplete="off">
                </div>
                <div>
                    <label>Postcode:</label><br/>
                    <input type="text" name="postcode" placeholder="Postcode" style="width: 50%;" autocomplete="off">
                </div>
                <div>
                    <label>Town / City:</label><br/>
                    <input type="text" name="town" placeholder="Town / City" autocomplete="off">
                </div>
                      <button class="btn_4" name="s_address" type="submit">Save</button>                      
                  </form>
                <hr style="width: 100%;">
            </div>
            <div class="pro_widget" style="flex-basis: 300px;" id="pro_phone">
                <form method="POST">
                    <input type="tel" name="phone" placeholder="Enter the new number."/>
                    <button class="btn_4" name="s_phone" type="submit">Save</button>                      
                </form>
                <hr style="width: 100%;">
            </div>
            <div class="pro_widget" style="flex-basis: 300px;" id="pro_email">
                <form method="POST">
                    <input type="text" name="email" placeholder="Enter new Email address."/>
                    <button class="btn_4" name="s_email" type="submit">Save</button>                   
                </form>
                <hr style="width: 100%;">
            </div>
            
<!--====================================================================================================-->
        </div>
        <div class="pro_main">
            <div class="pro_widget" style="flex-basis: 60px;">
                <p>Email:</p>
            </div>
            <div class="pro_widget">
                <span><?php echo escape($user->data()->zt_email)?> </span>
                <a href="#" class="edit_button_email"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                <p style="font-size: 10px;">Edit</p></a>
            </div>
            <hr style="width: 100%;"> 
            <!--======================================================-->
        </div>
        <div class="pro_main">
            <div class="pro_widget" style="flex-basis: 60px;">
                <p>Tel:</p>
            </div>
            <div class="pro_widget">
                <span style="text-decoration:none; color: #ffffff;"><?php echo escape($user->data()->phone)?></span>
                <a href="#" class="edit_button_phone"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                <p style="font-size: 10px;">Edit</p></a>
            </div>
            <hr style="width: 100%;"> 
            <!--======================================================-->
        </div>                            
        <div class="pro_main">
            <div class="pro_widget" style="flex-basis: 300px;">
                <span>Change Address:</span>
                <a href="#" class="edit_button_address"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                    <p style="font-size: 10px;">Edit</p></a>
            </div>
            <hr style="width: 100%;"> 
            <!--======================================================-->
        </div>
        <div class="pro_main">
            <div class="pro_widget" style="flex-basis: 300px;">
                <span>Change Password:</span>
                <a href="#" class="edit_button_pass"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                    <p style="font-size: 10px;">Edit</p></a>
            </div>
            <hr style="width: 100%;"> 
            <!--======================================================-->
        </div>
<?php require_once 'overall/footer.php';?>